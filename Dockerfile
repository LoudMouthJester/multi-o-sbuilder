FROM archlinux

RUN <<EOF
set -e

unifont_ver="15.1.04"
unifont_sha256="88e00954b10528407e62e97ce6eaa88c847ebfd9a464cafde6bf55c7e4eeed54"

pacman -Syu --noconfirm
pacman -S --noconfirm base-devel freetype2 fuse3 git python ttf-dejavu gptfdisk dosfstools squashfs-tools nano bash-completion less

curl https://ftp.gnu.org/gnu/unifont/unifont-${unifont_ver}/unifont-${unifont_ver}.bdf.gz -o unifont.bdf.gz

if ! echo ${unifont_sha256} unifont.bdf.gz | sha256sum --check - ; then
	exit 1
else
	mkdir -p /usr/share/fonts/unifont
	gzip -d unifont.bdf.gz
	mv unifont.bdf /usr/share/fonts/unifont/unifont.bdf
fi

EOF